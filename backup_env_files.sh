#!/bin/bash

##############################################################################
# Script to move .env files to another directory and keep the same file tree #
##############################################################################

##########################################################################
#FUNCTIONS                                                               #
##########################################################################

function Help
{   
    echo "Usage: $0 [Options]"
    echo "The first argument is the path to the diretory to watch"
    echo
    echo "The second argument is the path to the destination directory"    
}

function Copy_files
{
    DIRECTORY="$1"
    DESTDIRECTORY="$2"
    COUNT=0
    echo "START MOVING ENV FILES ..."
    for line in $(find "$DIRECTORY" -name "*.env"); do #find "$DIRECTORY" -name "*.env" | while read line; do
        DIRNAME="$(dirname "$line")"
        NEW_DIRECTORY="$DESTDIRECTORY""${DIRNAME#"$DIRECTORY"}"
        mkdir -p "$NEW_DIRECTORY"
        echo "move $line to $NEW_DIRECTORY"
        cp "$line" "$NEW_DIRECTORY"
        ((COUNT++))
    done
    echo "FINISH"
    echo "$COUNT files have been moved"
}

function Remove_last_slash
{
    NAME=$1
    if [[ "${NAME: -1}" == "/" ]]
    then
        NAME="${1%/}"
    fi
    echo "$NAME"
}

function Check_directories()
{
    DIRECTORY=$(Remove_last_slash "$1")
    DESTDIRECTORY=$(Remove_last_slash "$2")

    if [ ! -d "$DIRECTORY" ]
    then
        echo "Directory ""$DIRECTORY"" doesn't exist"
        return 1
    fi

    if [ ! -d "$DESTDIRECTORY" ]
    then
        echo "Create destination : ""$DESTDIRECTORY"" ..."
        mkdir -p "$DESTDIRECTORY"
    fi
}


################################################################
#Return help                                                   #
################################################################
while getopts ":h" opt; do
  case ${opt} in
    h )
      Help
      return
      ;;
    \? )
      echo "Option invalide: -$OPTARG" 1>&2
      return
      ;;
  esac
done

################################################################
#Main                                                          #
################################################################
Check_directories "$1" "$2"

if [ $? -ne 1 ]
then
    Copy_files "$1" "$2"
fi 
