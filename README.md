# Always find your .env files

## Description

Small bash script to get the .env files in a given folder.  
The files and the tree structure are then copied to a chosen folder.  
The script is recursive

## Usage

First you need to do 
```bash
chmod u+x backup_env_files.sh
```
To use the script just do
```bash
source backup_env_files.sh <argument1> <argument2>
```
argument1 = the directory to watch  
argument2 = the directory where files will be copied

